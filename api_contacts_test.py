import copy
import operator

from contacts import Contact, Email
from db import db

from test import DrugDevTestCase

def sorted_contact(contact):

    def sorted_emails(emails):
        return sorted(emails, key=operator.itemgetter("address"))

    return {
        k: (sorted_emails(v) if k == "emails" else v)
        for k, v in contact.items()
    }


def sorted_contacts(contacts):
    return sorted(
        [sorted_contact(c) for c in contacts],
        key=operator.itemgetter("username"))

def msg(msg):
    return {
        "message": msg
    }

class Case(DrugDevTestCase):

    def assertContact(self, json_contact, response):
        self.assertEqual(200, response.status_code)
        self.assertEqual(json_contact, sorted_contact(response.json))

    def assertContacts(self, json_contacts, response):
        self.assertEqual(200, response.status_code)
        self.assertEqual(json_contacts, sorted_contacts(response.json))

    def assertUnchangedApiState(self):
        response = self.client.get("/contacts")
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            [self.j1_frozen_copy, self.j2_frozen_copy],
            response.json)

    def setUp(self):
        super().setUp()

        self.c1 = Contact(
            username="user1",
            first_name="Primus",
            last_name="Amicus",
            emails=[
                Email(address="primus@amicus.org"),
            ])
        self.c2 = Contact(
            username="user2",
            first_name="Secundus",
            last_name="Amicus",
            emails=[
                Email(address="secundus@amicus.org"),
                Email(address="xnumber2@distopia.gov"),
            ])
        self.c3 = Contact(
            username="user3",
            first_name="Tertium",
            last_name="Amicus")

        self.j1 = {
            "username": "user1",
            "first_name": "Primus",
            "last_name": "Amicus",
            "emails": [
                { "address": "primus@amicus.org" },
            ]
        }
        self.j2 = {
            "username": "user2",
            "first_name": "Secundus",
            "last_name": "Amicus",
            "emails": [
                { "address": "secundus@amicus.org" },
                { "address": "xnumber2@distopia.gov" },
            ]
        }
        self.j3 = {
            "username": "user3",
            "first_name": "Tertium",
            "last_name": "Amicus",
            "emails": [],
        }

        # These copies are not actually "frozen" by the language semantics,
        # we just agree that we won't modify them.
        self.j1_frozen_copy = copy.deepcopy(self.j1)
        self.j2_frozen_copy = copy.deepcopy(self.j2)

        db.session.add_all([self.c1, self.c2])
        db.session.commit()

    def test_contacts_get(self):
        response = self.client.get("/contacts")
        self.assertContacts([self.j1, self.j2], response)

    def test_contacts_post_success_empty_emails(self):
        response = self.client.post("/contacts", json=self.j3)
        self.assertContact(self.j3, response)

        response = self.client.get("/contacts")
        self.assertContacts([self.j1, self.j2, self.j3], response)

    def test_contacts_post_success_some_emails(self):
        self.j3["emails"] = [
            { "address": "tertium@amicus.org" },
            { "address": "xnumber3@utopia.gov" },
        ]
        response = self.client.post("/contacts", json=self.j3)
        self.assertContact(self.j3, response)

        response = self.client.get("/contacts")
        self.assertContacts([self.j1, self.j2, self.j3], response)

    def test_contacts_post_success_empty_names(self):
        self.j3["first_name"] = ""
        self.j3["last_name"] = ""
        response = self.client.post("/contacts", json=self.j3)
        self.assertContact(self.j3, response)

        response = self.client.get("/contacts")
        self.assertContacts([self.j1, self.j2, self.j3], response)

    def test_contacts_post_fail_bad_username(self):
        self.j1["username"] = "u1" # too short
        response = self.client.post("/contacts", json=self.j1)
        self.assertEqual(400, response.status_code)
        self.assertUnchangedApiState()

        self.j1["username"] = "white space"
        response = self.client.post("/contacts", json=self.j1)
        self.assertEqual(400, response.status_code)
        self.assertUnchangedApiState()

    def test_contacts_post_fail_bad_email(self):
        self.j3["emails"] = [{ "address": "" }]
        response = self.client.post("/contacts", json=self.j3)
        self.assertEqual(400, response.status_code)
        self.assertUnchangedApiState()

        self.j3["emails"] = [{ "address": "x" }]
        response = self.client.post("/contacts", json=self.j3)
        self.assertEqual(400, response.status_code)
        self.assertUnchangedApiState()

        self.j3["emails"] = [{ "address": "x@y" }]
        response = self.client.post("/contacts", json=self.j3)
        self.assertEqual(400, response.status_code)
        self.assertUnchangedApiState()

    def test_contacts_post_duplicate_emails(self):
        self.j1["emails"] += self.j1["emails"]
        response = self.client.post("/contacts", json=self.j1)
        self.assertEqual(400, response.status_code)
        self.assertUnchangedApiState()

    def test_contacts_post_fail_conflicting_username(self):
        response = self.client.post(
            "/contacts",
            json={
                "username": "user1",
                "first_name": "Foo",
                "last_name": "Bar",
                "emails": [],
            })
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            msg("Contact 'user1' or some of the e-mails already exist"),
            response.json)
        self.assertUnchangedApiState()

    def test_contacts_post_fail_conflicting_email(self):
        self.j3["emails"].append({ "address": "secundus@amicus.org" })
        response = self.client.post("/contacts", json=self.j3)
        self.assertEqual(400, response.status_code)
        self.assertEqual(
            msg("Contact 'user3' or some of the e-mails already exist"),
            response.json)
        self.assertUnchangedApiState()

    def test_contacts_post_fail_incomplete_body(self):
        del self.j1["username"]
        response = self.client.post("/contacts", json=self.j1)
        self.assertEqual(400, response.status_code)
        self.assertUnchangedApiState()

        del self.j2["first_name"]
        response = self.client.post("/contacts", json=self.j2)
        self.assertEqual(400, response.status_code)
        self.assertUnchangedApiState()

        del self.j3["last_name"]
        response = self.client.post("/contacts", json=self.j3)
        self.assertEqual(400, response.status_code)
        self.assertUnchangedApiState()

        self.j1["username"] = "pilates"
        del self.j1["emails"]
        response = self.client.post("/contacts", json=self.j1)
        self.assertEqual(400, response.status_code)
        self.assertUnchangedApiState()

    def test_contact_get_success(self):
        response = self.client.get("/contacts/user1")
        self.assertContact(self.j1, response)
        self.assertUnchangedApiState()

    def test_contact_get_fail(self):
        response = self.client.get("/contacts/nobody")
        self.assertEqual(404, response.status_code)
        self.assertUnchangedApiState()

    def test_contact_put_success_change_first_name(self):
        self.j1["first_name"] = "Biggus"
        response = self.client.put("/contacts/user1", json=self.j1)
        self.assertContact(self.j1, response)

        response = self.client.get("/contacts")
        self.assertContacts([self.j1, self.j2], response)

    def test_contact_put_success_add_email(self):
        self.j1["emails"].append({ "address": "xnumber1@distopia.gov" })
        response = self.client.put("/contacts/user1", json=self.j1)
        self.assertContact(self.j1, response)

        response = self.client.get("/contacts")
        self.assertContacts([self.j1, self.j2], response)

    def test_contact_put_success_replace_emails(self):
        self.j1["emails"] = [
            { "address": "pilates@rome.gov" },
            { "address": "xnumber1@distopia.gov" },
        ]
        response = self.client.put("/contacts/user1", json=self.j1)
        self.assertContact(self.j1, response)

        response = self.client.get("/contacts")
        self.assertContacts([self.j1, self.j2], response)

    def test_contact_put_fail_incomplete_body(self):
        del self.j1["username"]
        response = self.client.put("/contacts/user1", json=self.j1)
        self.assertEqual(400, response.status_code)
        self.assertUnchangedApiState()

        del self.j2["first_name"]
        response = self.client.put("/contacts/user2", json=self.j2)
        self.assertEqual(400, response.status_code)
        self.assertUnchangedApiState()

        del self.j3["last_name"]
        response = self.client.put("/contacts/user3", json=self.j3)
        self.assertEqual(400, response.status_code)
        self.assertUnchangedApiState()

    def test_contact_put_fail_updated_username(self):
        self.j1["username"] = "pilates"
        response = self.client.put("/contacts/user1", json=self.j1)
        self.assertEqual(400, response.status_code)
        self.assertEqual(msg("Updating the username is not allowed"), response.json)
        self.assertUnchangedApiState()

    def test_contact_put_fail_bad_email(self):
        self.j1["emails"] = [{ "address": "x@y" }]
        response = self.client.put("/contacts/user1", json=self.j1)
        self.assertEqual(400, response.status_code)
        self.assertUnchangedApiState()

    def test_contact_patch_success_first_name(self):
        self.j1["first_name"] = "Biggus"
        response = self.client.patch("/contacts/user1", json={"first_name": "Biggus"})
        self.assertContact(self.j1, response)

        response = self.client.get("/contacts")
        self.assertContacts([self.j1, self.j2], response)

    def test_contact_patch_success_last_name(self):
        self.j1["last_name"] = "Cohen"
        response = self.client.patch("/contacts/user1", json={"last_name": "Cohen"})
        self.assertContact(self.j1, response)

        response = self.client.get("/contacts")
        self.assertContacts([self.j1, self.j2], response)

    def test_contact_patch_success_emails(self):
        self.j1["emails"] = [{ "address": "pilates@rome.gov" }]
        response = self.client.patch("/contacts/user1", json=self.j1)
        self.assertContact(self.j1, response)

        response = self.client.get("/contacts")
        self.assertContacts([self.j1, self.j2], response)

    def test_contact_patch_fail_updated_username(self):
        response = self.client.patch("/contacts/user1", json={"username": "pilates"})
        self.assertEqual(400, response.status_code)
        self.assertEqual(msg("Updating the username is not allowed"), response.json)
        self.assertUnchangedApiState()

    def test_contact_patch_fail_bad_email(self):
        response = self.client.patch(
            "/contacts/user1",
            json={
                "emails": [{ "address": "x@y" }]
            })
        self.assertEqual(400, response.status_code)
        self.assertUnchangedApiState()

    def test_contact_delete_success(self):
        response = self.client.delete("/contacts/user1")
        self.assertEqual(204, response.status_code)

        response = self.client.get("/contacts")
        self.assertContacts([self.j2], response)

    def test_contact_delete_fail(self):
        response = self.client.delete("/contacts/pilates")
        self.assertEqual(404, response.status_code)
        self.assertUnchangedApiState()

    def test_contact_email_get_success(self):
        response = self.client.get("/contacts/email/primus@amicus.org")
        self.assertContact(self.j1, response)
        self.assertUnchangedApiState()

    def test_contact_email_get_fail(self):
        response = self.client.get("/contacts/email/x@y.com")
        self.assertEqual(404, response.status_code)
        self.assertUnchangedApiState()
