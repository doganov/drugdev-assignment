import flask_restplus

import api_contacts

_header = {
    "version": "1.0",
    "title": "DrugDev Technical Test",
    "description": "Contact management API",
    "contact_email": "doganov@gmail.com",
}

_endpoints = [
    ("/contacts", api_contacts.Collection, api_contacts.ns),
    ("/contacts/<string:username>", api_contacts.Element, api_contacts.ns),
    ("/contacts/email/<string:email>", api_contacts.EmailLookup, api_contacts.ns),
]

def _make_api():
    api = flask_restplus.Api(**_header)
    visited_namespaces = []
    for url, resource, ns in _endpoints:
        if ns not in visited_namespaces:
            visited_namespaces.append(ns)
            api.add_namespace(ns)
        ns.add_resource(resource, url)

    return api

api = _make_api()
