# DrugDev Technical Test

# Prerequisites
* Python 3.7+ (it will probably work on previous versions of Python 3, but it's not tested)
* pip
* Redis
* GNU make

# Setup
```
make init
```

# Run tests
```
make test
```

# Start the development server
```
make dev
```

# Start a Celery worker
```
make worker
```
