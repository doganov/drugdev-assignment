import unittest

from flask import current_app
from app import flask_app
from db import db

class DrugDevTestCase(unittest.TestCase):

    def setUp(self):
        super().setUp()

        self.app = flask_app
        self.app_context = self.app.app_context()
        self.client = self.app.test_client()
        self.app_context.push()

        db.drop_all()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

        self.app_context.pop()

        super().tearDown()
