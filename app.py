import os

from flask import Flask
from celery import Celery

from api import api
from db import db, migrate
import env

def make_flask_app(env_name):
    app = Flask(__name__)
    app.config.from_object(getattr(env, env_name))
    db.init_app(app)

    # For performance reasons, foreign key constraints are not enforced by
    # default in SQLite.  The user have to enable them explicitly on every
    # connection.  In an ideal world, SQLAlchemy's SQLite driver would support
    # controlling this pragma via an optional parameter in the connection URI.
    # Unfortunately, nobody thought of that and we are forced to tune the
    # connections ourselves in code.

    from sqlalchemy import event
    from sqlite3 import Connection as SQLiteConnection

    def on_connect(dbapi_connection, connection_record):
        if isinstance(dbapi_connection, SQLiteConnection):
            dbapi_connection.execute("PRAGMA foreign_keys=ON")

    event.listen(db.get_engine(app), "connect", on_connect)

    ###

    import contacts
    migrate.init_app(app, db)

    api.init_app(app)

    return app

def make_celery_app(flask_app):
    celery_app = Celery(flask_app.import_name)

    import celery_conf
    celery_app.config_from_object(celery_conf)

    # Apply environment-specific configuration
    celery_app.conf.update({
        "broker_url": flask_app.config["CELERY_BROKER_URL"],
        "result_backend": flask_app.config["CELERY_RESULT_BACKEND"],
    })

    class FlaskTask(celery_app.Task):
        def __call__(self, *args, **kwargs):
            with flask_app.app_context():
                return self.run(*args, **kwargs)

    celery_app.Task = FlaskTask

    return celery_app

flask_env = os.getenv("FLASK_ENV")
if not flask_env:
    raise KeyError("FLASK_ENV is not set")

flask_app = make_flask_app(flask_env)
celery_app = make_celery_app(flask_app)

if __name__ == "__main__":
    flask_app.run()
