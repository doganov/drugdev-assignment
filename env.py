from os import path

BASE_DIR = path.abspath(path.dirname(__file__))

class Environment:
    DEBUG = False
    TESTING = False

    SQLALCHEMY_DATABASE_URI = None
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    CELERY_BROKER_URL = "redis://localhost:6379"
    CELERY_RESULT_BACKEND = "redis://localhost:6379"

class development(Environment):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + path.join(BASE_DIR, "dev.db")

class testing(Environment):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + path.join(BASE_DIR, "test.db")

class production(Environment):
    DB_URI = "TODO"
