from flask_restplus import Namespace, Resource, fields
import sqlalchemy as sqla

from contacts import Contact, Email
from db import db

ns = Namespace("contacts", path="/")

_username_re = r"^[0-9a-z_]{3,64}$"
_email_re = r"^.+@.+[.].+$"

EmailApiModel = ns.model("Email", {
    "address": fields.String(pattern=_email_re, required=True),
})

ContactApiModel = ns.model("Contact", {
    "username": fields.String(pattern=_username_re, required=True),
    "first_name": fields.String(required=True),
    "last_name": fields.String(required=True),
    "emails": fields.List(fields.Nested(EmailApiModel), required=True),
})

PartialContactApiModel = ns.model("PartialContact", {
    "username": fields.String(pattern=_username_re),
    "first_name": fields.String,
    "last_name": fields.String,
    "emails": fields.List(fields.Nested(EmailApiModel)),
})

class Collection(Resource):

    @ns.marshal_list_with(ContactApiModel)
    def get(self):
        """List all contacts"""
        return Contact.query.all()

    @ns.expect(ContactApiModel, validate=True)
    @ns.marshal_with(ContactApiModel)
    def post(self):
        """Create new contact"""
        emails_json = ns.payload.pop("emails")
        contact = Contact(**ns.payload)
        contact.emails = [Email(**json) for json in emails_json]
        try:
            db.session.add(contact)
            db.session.commit()
            return contact
        except (sqla.exc.IntegrityError, sqla.orm.exc.FlushError):
            db.session.rollback()
            username = ns.payload["username"]
            ns.abort(
                400,
                "Contact %r or some of the e-mails already exist" % username)

class Element(Resource):

    def _get_or_abort(self, username):
        try:
            return Contact.query.filter_by(username=username).one()
        except sqla.orm.exc.NoResultFound:
            ns.abort(404, "Contact %r not found" % username)

    def _update(self, contact, payload):
        exclude = ["username", "emails"]
        for key, value in payload.items():
            if key not in exclude:
                setattr(contact, key, value)
        if "emails" in payload:
            contact.emails = [Email(**json)for json in payload["emails"]]

    def _commit_or_abort(self, contact):
        try:
            db.session.add(contact)
            db.session.commit()
        except (sqla.exc.IntegrityError, sqla.orm.exc.FlushError):
            db.session.rollback()
            ns.abort(
                400,
                "Duplicate e-mails or some are already used by another contact")
        return contact

    @ns.marshal_with(ContactApiModel)
    def get(self, username):
        """Retrieve contact by username"""
        return self._get_or_abort(username)

    @ns.expect(ContactApiModel, validate=True)
    @ns.marshal_with(ContactApiModel)
    def put(self, username):
        """Update contact (full)"""

        if username != ns.payload["username"]:
            ns.abort(400, "Updating the username is not allowed")

        contact = self._get_or_abort(username)
        self._update(contact, ns.payload)
        self._commit_or_abort(contact)
        return contact

    @ns.expect(PartialContactApiModel, validate=True)
    @ns.marshal_with(ContactApiModel)
    def patch(self, username):
        """Update contact (partial)"""

        if username != ns.payload.get("username", username):
            ns.abort(400, "Updating the username is not allowed")

        contact = self._get_or_abort(username)
        self._update(contact, ns.payload)
        self._commit_or_abort(contact)
        return contact

    def delete(self, username):
        """Delete contact"""
        contact = self._get_or_abort(username)
        db.session.delete(contact)
        db.session.commit()
        return "", 204

class EmailLookup(Resource):

    def _get_or_abort(self, email):
        try:
            return Email.query.filter_by(address=email).one().contact
        except sqla.orm.exc.NoResultFound:
            ns.abort(404, "Contact with e-mail %r not found" % email)

    @ns.marshal_with(ContactApiModel)
    def get(self, email):
        """Retrieve contact by e-mail"""
        return self._get_or_abort(email)
