import logging
import time
from datetime import datetime, timedelta

import sqlalchemy as sqla

from app import celery_app
from db import db
from contacts import Contact, random_contact

log = logging.getLogger(__name__)

@celery_app.task
def contact_add_random():
    # Although unlikely, we may fail adding a random contact, because we could
    # get username or e-mail collision.  Still, we promised to add a contact, so
    # we'll try until we do (or fail for some other reason).

    added = False
    while not added:
        rando = random_contact()
        try:
            db.session.add(rando)
            db.session.commit()
            added = True
        except (sqla.exc.IntegrityError, sqla.orm.exc.FlushError) as e:
            log.error(e, exc_info=True)
            db.session.rollback()
            # We're retrying, but let's not create too hot loop here
            time.sleep(0.5)

    log.info("Added contact %r with %d e-mails", rando.username, len(rando.emails))

@celery_app.task
def contact_cleanup(seconds_to_live):
    threshold = datetime.utcnow() - timedelta(seconds=seconds_to_live)
    count = Contact.query.filter(Contact.time_created < threshold).delete()
    db.session.commit()
    log.info("Cleaned up %d contacts", count)
