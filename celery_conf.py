imports = ('tasks',)

beat_schedule = {
    "contact-add-random": {
        "task": "tasks.contact_add_random",
        "schedule": 15, # seconds
    },
    "contact-cleanup": {
        "task": "tasks.contact_cleanup",
        "args": (60,), # seconds of contact life until cleanup
        "schedule": 10, # seconds
    },
}
