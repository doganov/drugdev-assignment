SHELL=/bin/sh

.PHONY: test
test:
	FLASK_ENV=testing python -m unittest discover --pattern "*test.py" -v

.PHONY: dev
dev:
	FLASK_ENV=development flask run

.PHONY: worker
worker:
	FLASK_ENV=development celery worker --app app.celery_app --beat --loglevel=info

.PHONY: init
init:
	pip install -r requirements.txt
	FLASK_ENV=development flask db upgrade
