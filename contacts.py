import datetime
import random
import string

from db import db

_username_type = db.Unicode(64)

class Contact(db.Model):
    username = db.Column(db.Unicode(64), nullable=False, primary_key=True)
    first_name = db.Column(db.Unicode(128), nullable=False)
    last_name = db.Column(db.Unicode(128), nullable=False)
    time_created = db.Column(
        db.DateTime,
        nullable=False,
        default=datetime.datetime.utcnow)

    emails = db.relationship(
        "Email",
        backref="contact",
        cascade="all, delete-orphan",
        passive_deletes=True)

class Email(db.Model):
    username = db.Column(
        _username_type,
        db.ForeignKey("contact.username", ondelete="CASCADE"),
        nullable=False,
        primary_key=True)
    address = db.Column(
        db.Unicode(128),
        nullable=False,
        primary_key=True,
        unique=True)

def random_contact():
    def random_str(charset, size):
        return "".join(random.choices(charset, k=size))

    def lower(size):
        return random_str(string.ascii_lowercase, size)

    def letters(size):
        return random_str(string.ascii_letters, size)

    def email():
        return "%s@%s.%s" % (lower(8), lower(8), lower(3))

    return Contact(
        username=lower(10),
        first_name=letters(16),
        last_name=letters(16),
        emails=[
            Email(address=email())
            for _ in range(0, random.randint(0, 3))
        ])
